<?php 
include 'head.php'; 

if(!isset($_SESSION['login']) || !isset($_SESSION['client_name'])){
  //header('Location:index.php');
  echo '<script>window.location = "index.php"</script>';
}else{


require_once ('../app/lib/conexion.php');
conexion::conectar();

//Si se especifico un id de un boletin cargamos ese boletin
if (isset($_GET['id'])) {

    $bol_id = $_GET['id'];
    $stmt=conexion::$db->prepare("SELECT * FROM boletines WHERE bol_id = $bol_id");

}else{ //Si no, Se saca la info del ultimo boletin segun la fecha

    $stmt=conexion::$db->prepare("SELECT * FROM boletines ORDER BY bol_fecha DESC LIMIT 0,1");  

}

$oBoletin=conexion::leerRegistro($stmt);
conexion::desconectar();


?>



<div id="contenido">
	
  <h2>Catafracto seccion de Clientes</h2>
  <img src="../img/div.jpg" width="946" height="9" />
  <?php if (isset($_GET['id'])): ?>
    <h3><?=$oBoletin['bol_titulo'].' - '.$oBoletin['bol_fecha'] ?></h3>
  <?php else: ?>
    <h3>Ultimo Boletín | <?=$oBoletin['bol_titulo'].' | '.$oBoletin['bol_fecha'] ?></h3>
  <?php endif ?>
  <div style="width:816px; background-color:white; position:relative;">
    <?=$oBoletin['bol_contenido']?>
  </div>
  
 

</div><!--contenido-->

<?php } //Si esta logueado ?>

<?php require_once('footer.php'); ?>
<?php 
include 'head.php'; 

if(!isset($_SESSION['login']) || !isset($_SESSION['client_name'])){
  //header('Location:index.php');
  echo '<script>window.location = "index.php"</script>';
}else{


require_once '../app/lib/conexion.php';

conexion::conectar();

$stmt = conexion::$db->prepare('SELECT bol_id, bol_fecha, bol_titulo FROM boletines');
$oBoletines = conexion::leerTabla($stmt);

conexion::desconectar();


?>


<div id="contenido2">
  <div style="padding-top:23px; padding-bottom:20px;">
     <div class="clear">
       <span class="Titulos">DEVELOPMENT OF HIGH-TECH SECURITY</span> 
       <img src="../img/div.jpg" /> <br /> <br />
       <span style="color:#720305">Integrated systems high-tech security in the following areas:</span>
       <div class="clear margen" style="padding-bottom:20px;">
             <br />
            <ul style="margin:0; padding:0;">
              <li style="padding-bottom:20px;">Telecommunications</li>
              <li style="padding-bottom:20px;">Unmanned aerial vehicle</li>
              <li style="padding-bottom:20px;">Computer systems of intelligence</li>
              <li style="padding-bottom:20px;">Electronic security systems</li>
              <li style="padding-bottom:20px;">Network security systems</li>
              <li style="padding-bottom:20px;">protective systems of data voice and video</li>
              <li style="padding-bottom:20px;">Sistemas de seguimiento y ubicación de personas, vehículos y equipos</li>
            </ul>
       </div>
    </div>
  </div>
</div>

<script type="text/javascript"> 
  $(document).ready(function() { 
    $("table#TBclientes").tablesorter({ sortList: [[1,1]] });
    $("table#TBclientes").tablesorterPager({container: $("#pager"),positionFixed: false}); 
  }); 
</script>

<?php } //Si esta logueado ?>
<?php require_once('footer.php'); ?>
<?php 
include 'head.php'; 

if(!isset($_SESSION['login']) || !isset($_SESSION['client_name'])){
  //header('Location:index.php');
  echo '<script>window.location = "index.php"</script>';
}else{


require_once '../app/lib/conexion.php';

conexion::conectar();

$stmt = conexion::$db->prepare('SELECT bol_id, bol_fecha, bol_titulo FROM boletines');
$oBoletines = conexion::leerTabla($stmt);

conexion::desconectar();


?>


<div id="contenido">
  <h2>Lista de Boletines</h2>
  
  <table class="table table-bordered" id="TBclientes">
    <thead>      
    <tr>
      <th>Id</th>
      <th>Fecha</th>
      <th>Titulo</th>
      <th>Ver</th>      
    </tr>
    </thead>
    <?php foreach ($oBoletines as $key => $value): ?>      
    <tr>
      <td><?=$value['bol_id']?></td>
      <td><?=$value['bol_fecha']?></td>
      <td><?=$value['bol_titulo']?></td>      
      <td><a href="home.php?id=<?php echo $value['bol_id'];?>">Ver</a></td>
    </tr>
    
    <?php endforeach; ?>
  </table>
  
  <img src="../img/div.jpg" width="946" height="9" />
  
  <div id="pager" style="margin-top:10px">
    <form>
        <img src="../img/icons/first.png" class="first">
        <img src="../img/icons/prev.png" class="prev">
        <input type="text" class="pagedisplay">
        <img src="../img/icons/next.png" class="next">
        <img src="../img/icons/last.png" class="last">
        <select class="pagesize">
          <option selected="selected" value="10">10</option>
          <option value="20">20</option>
          <option value="30">30</option>
          <option value="40">40</option>
        </select>
    </form>
  </div>

</div><!--contenido-->

<script type="text/javascript">	
	$(document).ready(function() { 
    $("table#TBclientes").tablesorter({ sortList: [[1,1]] });
    $("table#TBclientes").tablesorterPager({container: $("#pager"),positionFixed: false}); 
  }); 
</script>

<?php } //Si esta logueado ?>
<?php require_once('footer.php'); ?>
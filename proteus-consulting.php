<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Catafracto | Dynamic Communications</title>
	<link href="css/estilo.css" rel="stylesheet" type="text/css" />


	<style>
		img.banner{
			width: 100%;
		}
		a.btn, a.btn:hover, a.btn:focus, a.btn:active, a.btn:visited{
			color:#fff;
			text-decoration: none;
			outline: none;
			margin: 20px 0;
			display: inline-block;
			font-size: 14px;
			padding: 7px 15px;
			border-radius: 3px;
			background: #479092;
		}
		h2{
			color: #222;
			margin: 20px 0;
		}
		hr{
			margin: 20px 0;
			height: 1px;
			background: #ccc;
			border: 0;
		}
		p b{
			color: #333;
		}
		.intro{
			border-bottom: 1px solid #ccc;
			overflow: hidden;
			text-align: justify;
			position: relative;
		}
		.intro p{
			font-size: 16px;
			line-height: 22px;
		}
		.intro img{
			height: 25px;
			display: inline-block;
			margin: 30px 0 10px 0;
		}
		.intro .site-link{
			position: absolute;
			top: 35px;
			right: 0;
			text-decoration: none;
			color: #222;
			font-weight: bold;
			border-bottom: 1px solid #aaa;
		}
		.w--100{
			width: 100%;
		}
		.content{
			padding: 20px 0;
		}
		.row{
			margin: 0 -15px;
			overflow: hidden;
		}
		.col-sm-6{
			width: calc(50% - 30px);
			float: left;
			padding: 0 15px;
		}
		.col-sm-3{
			width: calc(33% - 30px);
			float: left;
			padding: 0 15px;
		}
		.text-right{
			text-align: right;
		}
		.text-left{
			text-align: left;
		}
		footer{
			margin-top: 20px;
			border-top:1px solid #ccc;
			padding-top: 10px;
		}
		.p--color{
			color: #479092;
		}
		ul{
			margin: 0;
			padding: 0;
		}
	</style>
</head>

<body>
	<header id="header">
		<img src="img/logo.png" vspace="5" />
	</header>
	<?php require_once('menu.php'); ?>
	<div id="contenido2">
		<!-- Proteus Consulting -->
		<div class="row">
			<div class="col-sm-6">
				<img src="img/proteus-consulting/Logo-Proteus-Consulting.png" class="banner" alt="Logo Proteus Consulting" style="height: 60px; width: auto !important" />
			</div>
			<div class="col-sm-6 text-right">
				<h3 class="p--color" style="position: relative; top: 15px">Binational Risk Management Specialist</h3>
			</div>
		</div>
		<hr />
		<div class="content">
			<div class="row">
				<div class="col-sm-6">
					<h2 style="margin: 0 0 30px 0">EXECUTIVE SECURITY TRANSPORTATION</h2>
					<p>We provide turnkey secure transportation solutions for your single VIP or group of VIP’s.</p>

					<p>From one day visits to extended Ex-Pat deployments.</p>

					<p>We pick you up at the border or bring you across into Mexico and take you back home everyday, safely and expeditiously.</p>
				</div>
				<div class="col-sm-6">
					<img src="img/proteus-consulting/banners/banner7.jpg" class="w--100">
					<a href="http://proteusconsulting.com/service/executive-security-transportation" target="_blank" class="btn">Read more</a>
				</div>
			</div>
		</div>
		<hr />
		<div class="content">
			<div class="row">
				<div class="col-sm-6">
					<img src="img/proteus-consulting/banners/banner9.jpg" class="w--100">
					<a href="http://proteusconsulting.com/service/executive-protection-and-security-details" target="_blank" class="btn">Read more</a>
				</div>
				<div class="col-sm-6">
					<h2 style="margin: 0 0 30px 0">EXECUTIVE PROTECTION AND SECURITY DETAILS</h2>
					<p>We specialize in protecting groups and individuals from multinational companies in Mexico.</p>
					<p>Managers crossing the border daily to perform their duties.</p>
					<p>High level executives visiting corporate manufacturing facilities.</p>
					<p>Key suppliers and business partners temporarily performing key specific functions, whether they stay in Mexico or go back everyday.</p>
					<p>High visibility VIP’s for special events and functions.</p>
				</div>
			</div>
		</div>
		<hr />
		<div class="content">
			<div class="row">
				<div class="col-sm-6">
					<h2 style="margin: 0 0 30px 0">CORPORATE SECURITY PROGRAMS FOR COMPANIES DOING BUSINESS IN MEXICO </h2>
					<p>We tailor security programs for our customers, based on their specific threat and risk atmospherics.</p>
					<p>Our strategies effectively mitigate your risk and allow you to focus on what is important for your business.</p>
				</div>
				<div class="col-sm-6">
					<img src="img/proteus-consulting/banners/banner5.jpg" class="w--100">
					<a href="http://proteusconsulting.com/service/corporate-security-programs-for-companies-doing-business-in-mexico" target="_blank" class="btn">Read more</a>
				</div>
			</div>
		</div>
		<hr />
		<div class="content">
			<div class="row">
				<div class="col-sm-6">
					<img src="img/proteus-consulting/banners/banner6.jpg" class="w--100">
					<a href="http://proteusconsulting.com/service/security-training" target="_blank" class="btn">Read more</a>
				</div>
				<div class="col-sm-6">
					<h2 style="margin: 0 0 30px 0">SECURITY TRAINING</h2>
					
					<ul>
						<li>Tactical Armed & Un-armed Training</li>
						<li>Personal Security Details Certification</li>
						<li>EMTR (Emergency Medical Tactical Response)</li>
						<li>Crime Prevention and Awareness Courses</li>
						<li>Whole Family Contingency Courses</li>
						<li>Tactical Evasive/Defensive Driver Training</li>
					</ul>
				</div>
			</div>
		</div>
		
		<footer>
			Proteus Consulting<small>™</small>
		</footer>
	</div>
	<?php require_once('footer.php'); ?>
</body>
</html>

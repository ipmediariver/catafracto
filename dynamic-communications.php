<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Catafracto | Dynamic Communications</title>
	<link href="css/estilo.css" rel="stylesheet" type="text/css" />


	<style>
		img.banner{
			width: 100%;
		}
		h2{
			color: #222;
			margin: 20px 0;
		}
		p b{
			color: #333;
		}
		.intro{
			border-bottom: 1px solid #ccc;
			overflow: hidden;
			text-align: justify;
			position: relative;
		}
		.intro p{
			font-size: 16px;
			line-height: 22px;
		}
		.intro img{
			height: 25px;
			display: inline-block;
			margin: 30px 0 10px 0;
		}
		.intro .site-link{
			position: absolute;
			top: 35px;
			right: 0;
			text-decoration: none;
			color: #222;
			font-weight: bold;
			border-bottom: 1px solid #aaa;
		}
		.solution{
			overflow: hidden;
			margin: 30px 0;
			text-align: justify;
		}
		.solution .icon{
			float: left;
			margin: 5px 25px 45px 0;
		}
		.solution h3{
			color: #333;
			margin: 0 0 15px 0;
		}
		.solution p{
			font-size: 15px;
			line-height: 21px;
			margin-bottom: 10px;
		}
		.solution a{
			text-decoration:none;
			color: #730406;
			font-size: 12px;
			display: inline-block;
			padding-bottom: 5px;
			border-bottom: 1px solid #ccc;
		}
		footer{
			margin-top: 20px;
			border-top:1px solid #ccc;
			padding-top: 10px;
		}
	</style>
</head>

<body>
	<header id="header">
		<img src="img/logo.png" vspace="5" />
	</header>
	<?php require_once('menu.php'); ?>
	<div id="contenido2">
		<!-- Dynamic Communications -->
		<img src="img/productMapBannerHome.png" class="banner" alt="Dynamic Communications Solutions" />
		
		<div class="intro">
			<img src="img/logo-dynamic.png" alt="Dynamic Communications Logo" />
			<a href="#" class="site-link">Go to official site</a>
			<p>For now more than 16 years, <b>Dynamic Communications™</b> has been an industry leader in Seamless International IP Telephony Applications, Data Networks and Structured Cabling Solutions. Our customers range from Small Businesses to Large Multinational Companies, across a variety of Vertical Markets.</p>
			<p>With 9 offices throughout Mexico, 3 in the US and our Partners in South America, we have the ability to tackle any size project in for our Multinational Customers who demand a Single Point of Contact for their solution design, purchase and deployment.</p>
			<p>Our Solution Portfolio has increased with time and we are proud that our products and Services Offering now includes Cloud Based Services and Software.</p>
		</div>

		<div class="solutions">
			<div class="solution">
				<img src="img/dynamic-icons/collaboration-solutions.png" class="icon" alt="Collaboration Solutions" />
				<h3>Collaboration Solutions</h3>
				<p>DYNACOM´s collaboration solutions are the most innovative on the market today, providing high interactivity among peers, at the office or around the world; meet, join, work, congregate, collaborate just with a touch of a button, seamlessly sharing your point of view and experiences within your respective colleagues in an always on connected synergetic group. </p>
				<a href="http://godynacom.com/collaboration.php" target="_blank">Read more</a>
			</div>

			<div class="solution">
				<img src="img/dynamic-icons/data-center.png" class="icon" alt="Data Center" />
				<h3>Data Center Solutions</h3>
				<p>Your network’s systems are a key component of your company’s operations, this is why it’s very important to have your systems at optimal speed, doesn’t matter if you have just a few computers in your network, an small site or a big infrastructure in your data center full of mission critical services, we are able to help you to tackle any installation, implementation, restructuration or enhancement.</p>
				<a href="http://godynacom.com/datacenter.php" target="_blank">Read more</a>
			</div>

			<div class="solution">
				<img src="img/dynamic-icons/its.png" class="icon" alt="ITS" />
				<h3>ITS</h3>
				<p>We understand that today’s activities rely completely in Information Technologies which are sustained in your cabling infrastructure, but is your structured cabling designed to hold all of your network´s traffic?, what’s the lifetime of your solution?, is your cabling certified?, have you experienced bottlenecks if the past months?, we can help you answer those questions with the help of one our experts on the field, he/she can help you leverage your network´s full potential providing consulting and support on the best practices in the industry.</p>
				<a href="http://godynacom.com/its.php" target="_blank">Read more</a>
			</div>

			<div class="solution">
				<img src="img/dynamic-icons/software-solutions.png" class="icon" alt="Software Solutions" />
				<h3>Software Solutions</h3>
				<p>Need help with your business´s Image and productivity, don´t look any further, our partners form IP Media River are a team of designers, web masters and app developers that can help you built your new corporate productivity tools from the ground up, with solutions based on the cloud all of your employees will be able to access the critical information any time anywhere from any web enabled device maintaining top levels of productivity.</p>
				<a href="http://godynacom.com/software.php" target="_blank">Read more</a>
			</div>

			<div class="solution">
				<img src="img/dynamic-icons/electronic-security-solutions.png" class="icon" alt="Electronic Security Solutions" />
				<h3>Electronic Security Solutions</h3>
				<p>Nowadays security is no longer a choice but a necessity, our security solutions offer the peace of mind of knowing that your business is always monitored with our without guards on duty, with the help of video analytics, perimeter access control, and business protection measurements, your company´s belongings are always secured.</p>
				<a href="http://godynacom.com/electronic.php" target="_blank">Read more</a>
			</div>

			<div class="solution">
				<img src="img/dynamic-icons/network-security-solutions.png" class="icon" alt="Network Security Solutions" />
				<h3>Network Security Solutions</h3>
				<p>Networks nowadays need to be secure against, malicious software, viruses and most importantly hackers. The way we envision a hacker is a kid at his mother´s garage, but this is so far from the truth, in recent times these are mafia driven, big corporate sponsored groups with only one goal in mind, steal your information and in the worst case scenario your financial information such as credit card, billing statements or other personal information for blackmailing reasons.</p>
				<a href="http://godynacom.com/network.php" target="_blank">Read more</a>
			</div>

			<div class="solution">
				<img src="img/dynamic-icons/access-wireless.png" class="icon" alt="Access and Wireless" />
				<h3>Access and Wireless</h3>
				<p>With the introduction of mobile technology to the office you know that wired connections are sometimes virtually or physically impossible, that´s mainly the reason of why wireless integration to your network’s growing plan is very important, knowing the correct wireless network and speed is very relevant when the implementation plan is at early stages and it’s key to your business operations in the future.</p>
				<a href="http://godynacom.com/wifi.php" target="_blank">Read more</a>
			</div>
		</div>
		<footer>
			Dynamic Communications<small>™</small>
		</footer>
	</div>
	<?php require_once('footer.php'); ?>
</body>
</html>

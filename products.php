<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Catafracto</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css" />
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>

<body>
<div id="header">
	<img src="img/logo.png" vspace="5" />
</div>
<?php require_once('menu-e.php'); ?>
<div id="contenido2">
	<div id="Bannercont">
		<?php require_once('banner.php'); ?>
	</div>
    <br /><span class="Titulos">PRODUCTS</span><br />
    <span class="Titulos"><img src="img/div.jpg" width="946" height="9" /></span><br />
  <span class="subtitulos">Lobbying Government Strategic.</span><br />
With over 15 years representing world class companies to government authorities, we have the knowledge and professional training, supported by the experience in design and implementation of operational, functional and legal strategies, required by our principals, to maintain or achieve competitive levels that the global market requires to remain as leaders in their field.<br /><br />
<span class="subtitulos">Comprehensive outsourcing in foreing trade operations and representation in terms of compliance with health regulations.</span><br />
• With an attitude of "100% Regulatory Compliance" we develop external support programs by which we audit, monitor, define internal control procedures and monitor the development of the maquiladora sector customs operation, maintaining the adequate level of operational irrigation which adds significant value to the operating cost of manufacturing.<br />
• Development of comprehensive regulatory compliance programs, based on the definition of operational and functional requirements of the entity and in the development of an implementation and maintenance program which provides the security and sustainability of the manufacturing operation.<br /><br />
<span class="subtitulos">Design of international logistics strategies.</span><br />
Taking into account the competitive advantages that Mexico offers the global manufacturing sector to other global markets as well as other leading countries in providing economic and functional competitive conditions for manufacturing, we design logistics strategic solutions that positively impact the operating cost of our customers, keeping their commitments to their respective clients.<br /><br />
<span class="subtitulos">Consulting in Manufacturing Operation.</span><br />
• Assessment of current status and proposal for the implementation of a future condition that meets your opportunities for improvement in cycle time reduction, inventory reduction, cost reduction, optimization of performance of labor (direct and indirect), optimization of performance of materials, reduction of internal and field defects, etc..<br />
• Redesign of processes in order to achieve ideal results, superior to those resulting from imitating their competition.<br />
• A high degree of  confidence to do so, because we have resources obtained in more than 30 years  of work, research and training with APICS, USC, California Institute of  Technology, LEI, Six Sigma Academy and Maynard Institute, among others.

</div>
<?php require_once('footer-e.php'); ?>




</div>
</div>


</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Catafracto</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css" />
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>

<body>	
<div id="header">
	<img src="img/logo.png" vspace="5" />
</div>
<?php require_once('menu.php'); ?>
<div id="contenido2">
	<div id="Bannercont">
		<?php require_once('banner.php'); ?>
	</div>
    
    
    <div style="padding-top:23px; padding-bottom:20px;">
    
     <h2>SOLUCIONES QUE OFRECEMOS</h2> 
  
     <div class="clear">
         <span class="Titulos">CABILDEO GUBERNAMENTAL ESTRATÉGICO</span> 
         <img src="img/div.jpg" /> <br />
          
         <div class="clear margen" style="padding-bottom:20px;">
              Con más de 15 años representando empresas de clase mundial ante autoridades gubernamentales, 
              contamos con el conocimiento y formación profesional que nos respalda la experiencia adquirida por el diseño y ejecución de estrategias 
              operacionales, funcionales y legales requeridas por nuestras representadas para mantener o alcanzar los niveles competitivos que el mercado 
              mundial requiere para mantenerse como líderes en su ramo.
         </div>
     </div>        
    
     <div class="clear" style="padding-bottom:20px;">
         <span class="Titulos">OUTSOURCING INTEGRAL </span>
         <img src="img/div.jpg" /> <br />
         <div class="clear margen">
              <ul style="margin:0; padding:0;"> 
              <li style="padding-bottom:20px;">
               En operaciones de comercio exterior y representación en materia de cumplimiento a regulación sanitaria, con una actitud de "100 % en Cumplimiento Regulatorio" desarrollamos programas de apoyo externo mediante el cual auditamos,monitoreamos y definimos procedimientos de control internos, es así como supervisamos el desarrollo de la operación aduanera 
              del sector maquilador, manteniendo el nivel de riesgo operativo adecuado el cual representa un significativo valor agregado al costo 
              operativo de la manufactura.</li>

              <li>Desarrollo de programas de Cumplimiento Regulatorio integral, basados en la definición de los requerimientos operacionales y 
              funcionales de la entidad y en el desarrollo de un programa de ejecución y mantenimiento mediante el cual proporciona seguridad 
              y sustentabilidad a la operación manufacturera.</li>
              </ul>
         </div>
     </div>
     
          <div class="clear">
         <span class="Titulos">DESARROLLO Y MANEJO DE NÓMINAS</span> 
         <img src="img/div.jpg" /> <br />
          
         <div class="clear margen" style="padding-bottom:20px;">
              Nos damos a la tarea de diseñar estrategias en el manejo y administración de nóminas, Tenemos diferentes esquemas y servicios que proporcionamos de acuerdo a las necesidades de cada empresa, buscando siempre un ahorro y beneficio para nuestros asociados. 
         </div>
     </div> 
     
     
     <div class="clear">
         <span class="Titulos">CREACIÓN DE ESTRATEGIAS LOGÍSTICAS INTERNACIONALES</span>
         <img src="img/div.jpg" />
         <div class="clear margen" style="padding-bottom:20px;">
              Tomando en cuenta las ventajas competitivas que México ofrece al sector manufacturero ante otros mercados a nivel global, diseñamos soluciones estratégicas y logísticas que impacten positivamente el costo operativo de nuestros asociados manteniendo el cumplimiento de los compromisos adquiridos con sus respectivos clientes.
         </div>
     </div>
          
     <div class="clear" style="padding-bottom:20px;">
         <span class="Titulos">CONSULTORÍA EN OPERACIÓN MANUFACTURERA</span>
         <img src="img/div.jpg" />
         <div class="clear margen">
              <ul style="margin:0; padding:0;">
              <li style="padding-bottom:20px;">
               Evaluamos su condición actual y desarrollamos una propuesta para la implementación de una condición futura que 
              atienda sus oportunidades de mejora en reducción de tiempo de ciclo, reducción de inventario, reducción de costo, optimización 
              de rendimiento de mano de obra (directa e indirecta), optimización de rendimiento de materiales, reducción de defectos internos y en el campo, etc.
              </li>

              <li style="padding-bottom:20px;"> Rediseño de procesos con el objetivo de lograr resultados ideales, superiores a los que resulten de imitar a su competencia.</li>
              
              <li>Un alto grado de confianza en lograrlo, pues contamos con recursos obtenidos en más de 30 años de trabajo, investigación y 
              entrenamientos con APICS, USC, California Institute of Technology, LEI, Six Sigma Academy y Maynard Institute entre otros.</li>
              </ul>
         </div>
     </div>
     
     <div class="clear" style="padding-bottom:20px;">
         <span class="Titulos">ADQUISICIONES, ARRENDAMIENTOS DE BIENES MUEBLES Y CONTRATACIÓN DE SERVICIOS DE CUALQUIER NATURALEZA (ADMINISTRACIÓN PÚBLICA)</span>
         <img src="img/div.jpg" />
         <div class="clear margen">
              <ul style="margin:0; padding:0;">
              <li style="padding-bottom:20px;">
                Revisión de convocatorias para licitaciones públicas, adjudicaciones directas, e invitación a cuando menos tres personas.
              </li>

              <li style="padding-bottom:20px;">Elaboración y/o revisión de propuestas en apego a bases de licitación, invitación a cuando menos tres personas y 
              adjudicación directa que lleve a cabo el sector público.</li>
              
              <li  style="padding-bottom:20px;"> Soluciones en la elaboración de documentos de carácter legal y administrativo requeridos en licitaciones públicas y procedimientos de invitación a cuando menos tres personas y                adjudicación directa.</li>
              
              <li style="padding-bottom:20px;">Manejo de los requisitos legales para ser objeto de adjudicación por actualizarse los supuestos de excepción a la licitación pública.</li>
              
              <li style="padding-bottom:20px;">Apoyo a pedidos y/o contratos derivados de licitaciones públicas y procedimientos de invitación a cuando menos tres personas y adjudicación directa.</li>
              
              <li style="padding-bottom:20px;">Desarrollo de procesos licitatorios con carácter nacional, internacional y bajo la cobertura de los Tratados de Libre Comercio.</li>
              </ul>
         </div>
     </div>
     
     <div class="clear">
         <span class="Titulos">SOLUCIONES FISCALES</span> 
         <img src="img/div.jpg" /> <br />
          
         <div class="clear margen" style="padding-bottom:20px;">
            <br />
             Desarrollo de soluciones en materia fiscal a través del cumplimiento regulatorio aplicable.
         </div>
     </div>
     
     
     
     
     
</div>






</div>
<?php include ('footer.php'); ?>
</div>


</body>
</html>

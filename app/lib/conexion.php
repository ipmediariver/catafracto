<?php

class conexion {
    /** @var PDO */
    public static $db;
    public static function conectar() {
        try {
            self::$db = new PDO('mysql:host=localhost;dbname=alberto_catafractodb', 'Alber_desarrollo', 'ipm3di@');
            self::$db->exec("set names utf8");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public static function leerTabla($stmt) {
        if (!$stmt->execute()) {
            throw new Exception(implode(" : ", $stmt->errorInfo()));
        }
        return $stmt->fetchAll();
    }

    public static function leerRegistro($stmt) {
        if (!$stmt->execute()) {
            throw new Exception(implode(" : ", $stmt->errorInfo()));
        }
        return $stmt->fetch();
    }
    
    public static function ejecutar($stmt) {
        if (!$stmt->execute()) {
            throw new Exception(implode(" : ", $stmt->errorInfo()));
        }
    }
    public static function desconectar() {
        self::$db = null;
    }
}
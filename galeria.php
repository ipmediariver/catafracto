<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
	<title>Catafracto | Galeria</title>
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />
    <!--BANNER-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="js/jqbanner.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/jqbanner.css" />
<!--FIN BANNER-->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script>
		!window.jQuery && document.write('<script src="jquery-1.4.3.min.js"><\/script>');
	</script>
	<script type="text/javascript" src="./fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />
 	<link rel="stylesheet" href="style.css" />
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			*   Examples - images
			*/

			$("a#example1").fancybox();

			$("a#example2").fancybox({
				'overlayShow'	: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});

			$("a#example3").fancybox({
				'transitionIn'	: 'none',
				'transitionOut'	: 'none'	
			});

			$("a#example4").fancybox({
				'opacity'		: true,
				'overlayShow'	: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'none'
			});

			$("a#example5").fancybox();

			$("a#example6").fancybox({
				'titlePosition'		: 'outside',
				'overlayColor'		: '#000',
				'overlayOpacity'	: 0.9
			});

			$("a#example7").fancybox({
				'titlePosition'	: 'inside'
			});

			$("a#example8").fancybox({
				'titlePosition'	: 'over'
			});

			$("a[rel=example_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});

			/*
			*   Examples - various
			*/

			$("#various1").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});

			$("#various2").fancybox();

			$("#various3").fancybox({
				'width'				: '75%',
				'height'			: '75%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

			$("#various4").fancybox({
				'padding'			: 0,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
		});
	</script>
</head>
<body>
<div id="header">
	<img src="img/logo.png" vspace="5" />
</div>
<?php require_once('menu.php'); ?>
<div id="contenido2">
	<div id="Bannercont">
		<div id="bannercontenedor">
    <div id="contbanner">
    <div class="banner">

<div id="jqb_object">
	
	<div class="jqb_slides">
		
        <div class="jqb_slide" title="" ><img src="img/banner1.jpg" title="" alt=""/></div>
        <div class="jqb_slide" title="" ><img src="img/banner2.jpg" title="" alt=""/></div>
	</div>

	<div class="jqb_bar"> 
		<div .class="jqb_info"></div>	
		<div id="btn_next" class="jqb_btn jqb_btn_next"></div>
		<div id="btn_pauseplay" class="jqb_btn jqb_btn_pause"></div>
		<div id="btn_prev" class="jqb_btn jqb_btn_prev"></div>
	</div>

</div>



</div>
</div>
</div>
	</div>
	<br />
    <span class="Titulos">GALERIA</span><span class="Titulos"><img src="img/div.jpg" width="946" height="9" /></span>
    <br />
    <div id="content">
	<p>
   		<a id="example1" href="./photos/image1.jpg"><img  src="./photos/thumb_image1.jpg" /></a>
		<a id="example1" href="./photos/image2.jpg"><img  src="./photos/thumb_image2.jpg" /></a>
		<a id="example1" href="./photos/image3.jpg"><img  src="./photos/thumb_image3.jpg" /></a>
		<a id="example1" href="./photos/image4.jpg"><img  src="./photos/thumb_image4.jpg" /></a>
        <a id="example1" href="./photos/image5.jpg"><img  src="./photos/thumb_image5.jpg" /></a>
        <a id="example1" href="./photos/image6.jpg"><img  src="./photos/thumb_image6.jpg" /></a>
        <a id="example1" href="./photos/image7.jpg"><img  src="./photos/thumb_image7.jpg" /></a>
        <a id="example1" href="./photos/image8.jpg"><img  src="./photos/thumb_image8.jpg" /></a>
        <a id="example1" href="./photos/image9.jpg"><img  src="./photos/thumb_image9.jpg" /></a>
        <a id="example1" href="./photos/image10.jpg"><img  src="./photos/thumb_image10.jpg" /></a>
        <a id="example1" href="./photos/image11.jpg"><img  src="./photos/thumb_image11.jpg" /></a>
        <a id="example1" href="./photos/image12.jpg"><img class="last" src="./photos/thumb_image12.jpg" /></a>
	</p>
	</div>
</div>
<?php require_once('footer.php'); ?>

</body>
</html>
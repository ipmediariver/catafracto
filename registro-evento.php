<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Catafracto</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="http://catafracto.com/favicon.ico">
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>

</head>

<body>
<div id="header">
	<img src="img/logo.png" vspace="5" />
</div>
<?php include('menu.php'); ?>
<div id="contenido">
	  <h1>II Foro Internacional de Comercio Exterior</h1>
    <p>Regístrate aquí para asistir!</p>
    <form action="app/frmContact.php" method="post" id="regForm" class="registerForm">
      <legend>Empresa</legend>
      <input type="text" name="txtEmpresa" placeholder="Su empresa" required>
      <legend>Nombre</legend>
      <input type="text" name="txtNombre" placeholder="Su nombre" required>
      <legend>Teléfono</legend>
      <input type="text" name="txtTel" placeholder="Su Telefono" required>
      <legend>Correo Electrónico</legend>
      <input type="text" name="txtEmail" placeholder="Su Email" required><br>
      <label for="reservacion"><input id="reservacion" name="reservacion" type="checkbox" style="width: 15px;height: 12px;" />Requiere reservación de Hotel</label>
      <hr />
      <legend>Si requiere facturacion, favor de proporcionar los siguientes datos:</legend><br />
      <legend>Nombre/Razon Social</legend>
      <input type="text" name="razonSocial" placeholder="Razon Social" />
      <legend>RFC</legend>
      <input type="text" name="rfc" placeholder="Su RFC" />
      <legend>Direccion Fiscal</legend>
      <input type="text" name="dir_fiscal" placeholder="Su domicilio fiscal" />
      <div>
      <button type="text">Registrate</button>
      </div>
    </form>
    <img src="img/logos-event.jpg" style="margin-top:15px;">
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#regForm').submit(function(){
          var self = $(this);
          var empresa = $('[name=txtEmpresa]').val();
          var nombre = $('[name=txtNombre]').val();

          if(nombre.length>1 && empresa.length>0){
            $.post('app/frmContact.php',$(this).serialize(),function(d){
                alert(d);
                $('#regForm > input').val('');
            });
          }else{
            alert('Ingresa tu nombre y el texto de la empresa');
          }
          
          return false;
        });
    });
</script>

<?php require_once('footer.php'); ?>

</div>
</div>


</body>
</html>

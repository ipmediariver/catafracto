<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Catafracto</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css" />
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>

<body>
<div id="header">
	<img src="img/logo.png" vspace="5" />
</div>
<?php require_once('menu-e.php'); ?>
<div id="contenido2">
	<div id="Bannercont">
		<?php require_once('banner.php'); ?>
	</div>
    
    
    <div style="padding-top:23px; padding-bottom:20px;">
  
     <div class="clear">
         <span class="Titulos">STRATEGIC GOVERNMENT LOBBYIST</span>
         <img src="img/div.jpg" />
         <div class="clear margen" style="padding-bottom:20px;">With over 15 years representing world class companies to government authorities, we have the knowledge and professional training, supported by the experience in the design and the implementation of operational, functional and legal strategies, required by our companies to maintain or achieve competitive levels that the global market requires to remain as leaders in their field.

         </div>
     </div>
     
     <div class="clear" style="padding-bottom:20px;">
         <span class="Titulos">INTEGRAL OUTSOURCING ON FOREIGN TRADE OPERATIONS AND REPRESENTATION IN TERMS 
                                                  OF SANITARY REGULATION COMPLIANCE.

</span>
         <img src="img/div.jpg" />
         <div class="clear margen">
              <ul style="margin:0; padding:0;">
              <li style="padding-bottom:20px;">
               With an attitude of "100% Regulatory Compliance" we develop programs of external support in which we audit, 
              monitor and define internal procedures, as we control the development of the customs operation of the “maquiladora” 
              sector, maintaining the adequate operational level, which adds significant value to the operating cost of manufacturing.
             </li>

              <li>  Development of integral trade compliance programs based on the definition of the functional and operational requirements 
              of the entity, these programs are based also in the development, implementation and maintenance which provide security and 
              sustainability to the manufacturing operation.</li>
              </ul>
         </div>
     </div>
     
     
     <div class="clear">
         <span class="Titulos">INTERNATIONAL LOGISTICS SOLUTIONS DESIGNS.</span>
         <img src="img/div.jpg" />
         <div class="clear margen" style="padding-bottom:20px;">
              Considering the competitive advantages on manufacturing sector that Mexico offers to other global markets, we design strategic and logistics solutions that impact positively the operating cost of our clients while maintaining compliance and commitments to their respective clients.


         </div>
     </div>
     
     
     <div class="clear" style="padding-bottom:20px;">
         <span class="Titulos">MANUFACTURING CONSULTING SERVICES.

</span>
         <img src="img/div.jpg" />
         <div class="clear margen">
              <ul style="margin:0; padding:0;">
              <li style="padding-bottom:20px;"> We evaluate the current status of our companies and develop proposals for the implementation of a future condition 
              for opportunities of improvement such as reduction of cycle time, inventory, cost, labor performance optimization, optimization 
              of materials, reduction of internal defects and in the field, etc.
              </li>

              <li style="padding-bottom:20px;"> We redesign the manufacturing processes with the purpose of achieving ideal results, superior to your competition.</li>
              
              <li> With 30 years of work we count with the resources, research and training of APICS, USC, California Institute of Technology, LEI, 
              Six Sigma Academy and Maynard Institute among others.</li>
              </ul>
         </div>
     </div>
     
     
</div>






</div>
<?php include ('footer-e.php'); ?>
</div>


</body>
</html>

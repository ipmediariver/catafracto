<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Catafracto</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css" />
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>

<body>
<div id="header">
	<img src="img/logo.png" vspace="5" />
</div>
<?php require_once('menu.php'); ?>
<div id="contenido2">
	<div id="Bannercont">
		<?php require_once('banner.php'); ?>
	</div>
    <br /><span class="Titulos">PRODUCTOS</span><br />
    <span class="Titulos"><img src="img/div.jpg" width="946" height="9" /></span><br />
  <span class="subtitulos">Cabildeo Gubernamental Estratégico</span><br />
Con más de 15 años representando empresas de Clase Mundial ante Autoridades Gubernamentales, contamos con el conocimiento y formación profesional que respalda la experiencia adquirida por el diseño y ejecución de estrategias operacionales, funcionales y legales requeridas por nuestras representadas para mantener o alcanzar los niveles competitivos que el mercado mundial demanda para continuar como líderes en su ramo.<br /><br />
<span class="subtitulos">Outsourcing Integral en Operaciones de Comercio Exterior y Representacion en materia de cumplimiento a Regulación Sanitaria.</span><br />
• Con una actitud de "100 % en Cumplimiento Regulatorio" desarrollamos programas de apoyo externo mediante el cual auditamos, monitoreamos , definimos procedimientos de control internos así como supervisamos el desarrollo de la operación aduanera del sector maquilador, manteniendo el nivel de riego operativo adecuado el cual agrega un significativo valor agregado al costo operativo de la manufactura.<br />
• Desarrollo de programas de cumplimiento regulatorio integral, basados en la definición de los requerimientos operacionales y funcionales de la entidad y en el desarrollo de un programa de ejecución y mantenimiento mediante el cual proporciona la seguridad y sustentabilidad de la operación manufacturera.<br /><br />
<span class="subtitulos">Creación de Estrategias logísticas internacionales.</span><br />
Tomando en cuenta las ventajas competitivas que México ofrece al sector manufacturero mundial ante otros mercados a nivel global así como otros países líderes en ofrecer condiciones competitivas económicas y funcionales para la manufactura, diseñamos soluciones estratégicas Logísticas que impacten positivamente la costo operativo de nuestros asociados manteniendo el cumplimiento de los compromisos adquiridos con sus respectivos clientes.<br /><br />
<span class="subtitulos">Consultora en operación Manufacturera.</span><br />
• Evaluación de su condición actual y propuesta para la implementación de una condición futura que atienda sus oportunidades de mejora en reducción de tiempo de ciclo, reducción de inventario, reducción de costo, optimización de rendimiento de mano de obra (directa e indirecta), optimización de rendimiento de materiales, reducción de defectos internos y en el campo, etc.<br />
• Rediseño de procesos con el objetivo de lograr resultados ideales, superiores a los que resulten de imitar a su competencia.<br />
• Un alto grado de confianza en lograrlo, pues contamos con recursos obtenidos en mas de 30 años de trabajo, investigación y entrenamientos con APICS, USC, California Institute of Technology, LEI, Six Sigma Academy y Maynard Institute entre otros.

</div>
<?php require_once('footer.php'); ?>




</div>
</div>


</body>
</html>

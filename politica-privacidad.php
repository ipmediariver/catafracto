<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Catafracto</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="http://catafracto.com/favicon.ico">
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>

</head>

<body>
<div id="header">
	<img src="img/logo.png" vspace="5" />
</div>
<?php include('menu.php'); ?>
<div id="contenido">

  <h1>POLÍTICA DE PRIVACIDAD Y MANEJO DE DATOS PERSONALES</h1>

  <p><b>En cumplimiento con lo establecido por la Ley Federal de Transparencia y Acceso a la 
  Información Pública Gubernamental te informamos nuestra política de privacidad y manejo 
  de datos personales y hacemos el siguiente compromiso:</b></p>
  
  <ul class="list">
  <li>Utilizamos tecnologías de Internet para administrar nuestro sitio Web y nuestros programas 
  de correo electrónico, pero no para reunir o almacenar información personal.</li>

  <li>Los datos que te solicitamos en los formularios de contacto o cualquier tipo de formulario 
  colocado dentro de nuestro Sitio Web únicamente serán utilizados para poder establecer contacto 
  contigo en relación a tu petición o comentario. 
  Los datos que ingreses en cualquiera de los formularios dentro del Sitio Web no serán difundidos, 
  distribuidos o comercializados.</li>

  <li>En caso de que desees ser removido de nuestra base de datos podrás, en cualquier momento, solicitar 
  la baja de la información mediante correo electrónico a 
  <a href="mailto:IIIForoInternacional2014@catafracto.com">IIIForoInternacional2014@catafracto.com</a>.</li>

  <li>Tu petición o comentario puede ser incluido dentro de los informes estadísticos que se elaboren para 
  el seguimiento de avances  de II Foro Internación de Comercio Exterior. No obstante, dichos informes 
  serán meramente estadísticos y no incluirán información que permita identificarte en lo individual.</li>
  </ul>

</div>

<?php require_once('footer.php'); ?>

</div>
</div>


</body>
</html>

<?php 
include 'head.php'; 

if(!isset($_SESSION['login']) || !isset($_SESSION['admin_name'])){
  //header('Location:index.php');
  echo '<script>window.location = "index.php"</script>';
}else{

  if(isset($_GET['id'])){
    $id_boletin = $_GET['id'];
    require_once '../app/lib/conexion.php';

    conexion::conectar();

    $stmt = conexion::$db->prepare("SELECT * FROM boletines WHERE bol_id = $id_boletin");
    $oBoletin = conexion::leerRegistro($stmt);

    conexion::desconectar();
  }
?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />

<div id="contenido" style="min-height:660px">
	
  <h2><?php if(isset($_GET['id'])){ echo 'Editar Boletin'  ; $flag=0; }else{ echo 'Nuevo Boletin'; $flag=1; } ?></h2>
  
  <img src="../img/div.jpg" width="946" height="9" />
  <!-- OF COURSE YOU NEED TO ADAPT ACTION TO WHAT PAGE YOU WANT TO LOAD WHEN HITTING "SAVE" -->          
  <form method="post" action="forms/frmBoletin.php">      
        <input type="hidden" name="bol_id" id="bol_id" value="<?php if(isset($_GET['id'])){ echo $oBoletin['bol_id']; } ?>">
        <label>Fecha de Boletín</label>
        <input type="text" name="bol_fecha" id="bol_fecha" value="<?php if(isset($_GET['id'])){ echo $oBoletin['bol_fecha']; } ?>" required>
        <label>Titulo de Boletín</label>
        <input type="text" name="bol_titulo" id="bol_titulo" value="<?php if(isset($_GET['id'])){ echo $oBoletin['bol_titulo']; } ?>" required>        
        <label>Contenido de Boletín</label>
        <textarea name="content" cols="50" rows="15">
          <?php if(isset($_GET['id'])){ echo $oBoletin['bol_contenido']; } ?>
          
        </textarea>
         <input type="hidden" id="flag" name="flag" value="<?=$flag;?>">
         <input type="hidden" id="id" name="id" value="<?=$_GET['id'];?>">
        <input type="submit" value="<?php if(isset($_GET['id'])){ echo 'Guardar Cambios'; }else{ echo 'Guardar'; } ?>" />
      </p>
  </form>
 

</div><!--contenido-->

<script type="text/javascript" src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
<script type="text/javascript" src="../js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
  
</script>
<script>
    $(document).ready(function(){
        tinyMCE.init({
              theme : "advanced",
              mode : "textareas",
              plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
              width : "816",
              height : "1056",
              theme_advanced_buttons3_add : "fullpage",
              // Theme options
              theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
              theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
              theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
              theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
              theme_advanced_toolbar_location : "top",
              theme_advanced_toolbar_align : "left",
              theme_advanced_statusbar_location : "bottom",
              theme_advanced_resizing : true,
              template_templates : [
                {
                        title : "Boletin Mensual",
                        src : "template_boletin.htm",
                        description : "Formato usado para los boletines mensuales"
                }
              ]
        });
                
        
        /*$( "#resizable" ).resizable().draggable();*/
        $( "#bol_fecha" ).datepicker({dateFormat: 'yy-mm-dd'});
        
    });    
</script>

<?php } //Si esta logueado ?>

<?php require_once('footer.php'); ?>
<?php 
include 'head.php'; 

if(!isset($_SESSION['login']) || !isset($_SESSION['admin_name'])){
  //header('Location:index.php');
  echo '<script>window.location = "index.php"</script>';
}else{


if(isset($_GET['id'])){ //si se va a editar
	require_once '../app/lib/conexion.php'; 

	//id del cliente
	$cid = $_GET['id'];	

	conexion::conectar();

	$stmt = conexion::$db->prepare("SELECT * FROM clientes WHERE cte_id = $cid");
	$oCliente = conexion::leerRegistro($stmt);

	conexion::desconectar();

}

?>


<div id="contenido">
  <?php if(isset($_GET['id'])): ?>
  <h2>Editar Cliente</h2>
  <?php $flag=0; ?>
  <?php else: ?>
  <h2>Nuevo Cliente</h2>  
  <?php $flag=1; ?>
  <?php endif; ?>
  <img src="../img/div.jpg" width="946" height="9" />
  <form action="forms/frmCliente.php" method="post" name="frmAddCliente" id="frmAddCliente">
  	<input type="hidden" name="cte_id" id="cte_id" value="<?php if(isset($_GET['id'])){ echo $cid; } ?>">
  	<label for="">Nombre</label>
  	<input type="text" name="cte_nombre" id="cte_nombre" placeholder="Nombre del Cliente" value="<?php if(isset($_GET['id'])){ echo $oCliente['cte_nombre']; } ?>">
  	<label for="">Email</label>
  	<input type="email" name="cte_email" id="cte_email" placeholder="Email del Cliente" value="<?php if(isset($_GET['id'])){ echo $oCliente['cte_email']; } ?>">
  	<label for="">Password</label>
  	<input type="password" name="cte_password" id="cte_password" placeholder="Password del Cliente" value="<?php if(isset($_GET['id'])){ echo $oCliente['cte_password']; } ?>">
  	<img src="../img/div.jpg" width="946" height="9" />
        <input type="hidden" id="flag" name="flag" value="<?=$flag;?>">
         <input type="hidden" id="id" name="id" value="<?=$_GET['id'];?>">
  	<button type="submit">Guardar Cliente</button>
  </form>
  

</div><!--contenido-->

<script type="text/javascript">	
	//Validaciones 	
	jQuery(function(){            
		jQuery("#cte_nombre").validate({
              expression: "if (VAL.length > 1 && VAL) return true; else return false;",
              message: "Ingrese el nombre del cliente"
        });

        jQuery("#cte_email").validate({
            expression: "if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;",
            message: "Ingrese un email valido"
        });
        jQuery("#cte_password").validate({
            expression: "if (VAL.length > 5 && VAL) return true; else return false;",
            message: "Porfavor ingrese una contrasena correcta"
        });
    });  
</script>

<?php } //Si esta logueado ?>

<?php require_once('footer.php'); ?>
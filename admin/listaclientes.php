<?php 
include 'head.php'; 

if(!isset($_SESSION['login']) || !isset($_SESSION['admin_name'])){
  //header('Location:index.php');
  echo '<script>window.location = "index.php"</script>';
}else{

?>

<?php 

require_once '../app/lib/conexion.php'; 

conexion::conectar();

$stmt = conexion::$db->prepare('SELECT * FROM clientes WHERE cte_tipo = 2');
$oClientes = conexion::leerTabla($stmt);

conexion::desconectar();


?>



<div id="contenido">
  <h2>Lista de Clientes</h2>
  
  <table class="table table-bordered" id="TBclientes">
    <thead>      
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Email</th>
      <th>Password</th>
      <th>Tipo</th>
      <th>Editar</th>
      <th>Borrar</th>
    </tr>
    </thead>
    <?php foreach ($oClientes as $key => $value): ?>      
    <tr>
      <td><?=$value['cte_id']?></td>
      <td><?=$value['cte_nombre']?></td>
      <td><?=$value['cte_email']?></td>
      <td><?=$value['cte_password']?></td>
      <td><?php if($value['cte_tipo']==1){echo 'Administrador';}else{echo 'Cliente';}?></td>
      <td><a href="cliente.php?id=<?php echo $value['cte_id'];?>">editar</a></td>
      <td><a href="#">borrar</a></td>
    </tr>
    
    <?php endforeach; ?>
  </table>
  
  <img src="../img/div.jpg" width="946" height="9" />
  
  <div id="pager" style="margin-top:10px">
    <form>
        <img src="../img/icons/first.png" class="first">
        <img src="../img/icons/prev.png" class="prev">
        <input type="text" class="pagedisplay">
        <img src="../img/icons/next.png" class="next">
        <img src="../img/icons/last.png" class="last">
        <select class="pagesize">
          <option selected="selected" value="10">10</option>
          <option value="20">20</option>
          <option value="30">30</option>
          <option value="40">40</option>
        </select>
    </form>
  </div>

</div><!--contenido-->

<script type="text/javascript">	
	$(document).ready(function() { 
    $("table#TBclientes").tablesorter({ sortList: [[4,0]] });
    $("table#TBclientes").tablesorterPager({container: $("#pager"),positionFixed: false}); 
  }); 
</script>

<?php } //Si esta logueado ?>
<?php require_once('footer.php'); ?>